package mn.oauth.controllers;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.security.token.jwt.render.BearerAccessRefreshToken;
import mn.oauth.clients.KeycloakUser;
import mn.oauth.to.User;
import mn.oauth.clients.KeycloakClient;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@Controller("/user")
@Secured(SecurityRule.IS_AUTHENTICATED)
public class UserController {

    @Inject
    KeycloakClient keycloakClient;

    @Get
    public HttpResponse<String> hi(@NotNull Principal principal) {
        return HttpResponse.ok("hi " + principal.getName());
    }

    @Post("/login")
    public HttpResponse<String> login(@Body User user) {
        KeycloakUser keycloakUser = new KeycloakUser();
        keycloakUser.setUsername(user.getLogin());
        keycloakUser.setPassword(user.getPassword());
        keycloakUser.setGrant_type("password");
        String token = keycloakClient.getToken(keycloakUser);
        return HttpResponse.ok("token:" + token);
    }
    
    @Get("/say/{token}")
    public HttpResponse<String> sayMyNameSayMyName(@PathVariable String token) {
        String userInfo = keycloakClient.getInfos("Bearer " + token);
        return HttpResponse.ok("token:" + userInfo);
    }

}
