package mn.oauth.clients;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.http.client.annotation.Client;

@Client("${app.auth.providers.keycloak.url}")
public interface KeycloakClient {

    @Post("/auth/realms/master/protocol/openid-connect/token")
    @Produces(MediaType.APPLICATION_FORM_URLENCODED)
    String getToken(@Body KeycloakUser user);

    @Get("/auth/realms/master/protocol/openid-connect/userinfo")
    String getInfos(@Header String authorization);
}
